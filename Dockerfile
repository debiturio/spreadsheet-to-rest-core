FROM php:cli
RUN apt-get update && apt-get install -y
RUN apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-install zip
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer