<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


class HeadlineMapExtractor
{
    public static function extract(SpreadsheetFileReaderInterface $reader, int $offset = 0): array
    {
        $result = [];

        foreach ($reader->getRows($offset,1) as $row) {
            foreach ($row->getCells() as $cell) {
                $result[$cell->getColumnIndex()] = $cell->getValue();
            }
        }

        return $result;
    }
}