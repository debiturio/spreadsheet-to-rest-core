<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


interface RowInterface
{
    public function getIndex(): int;

    public function getCells(): CellIteratorInterface;

    public function getCellByColumnIndex(int $columnIndex): ?CellInterface;
}