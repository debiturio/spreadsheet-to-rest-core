<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


interface CellIteratorInterface extends \Iterator
{
    public function current(): CellInterface;
}