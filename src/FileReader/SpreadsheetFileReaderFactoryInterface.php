<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


interface SpreadsheetFileReaderFactoryInterface
{
    public function getReader(string $filePath): SpreadsheetFileReaderInterface;
}