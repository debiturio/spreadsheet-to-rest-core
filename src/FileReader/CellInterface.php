<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


interface CellInterface
{
    public function getColumnIndex(): int;

    public function getValue(): mixed;
}