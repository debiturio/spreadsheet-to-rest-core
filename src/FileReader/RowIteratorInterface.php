<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


interface RowIteratorInterface extends \Iterator
{
    public function current(): RowInterface;
}