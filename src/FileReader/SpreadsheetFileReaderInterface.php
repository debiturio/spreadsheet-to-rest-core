<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\FileReader;


interface SpreadsheetFileReaderInterface
{
    public function getTotalRows(): int;

    public function getRows(int $offset = 0, int $rows = null): RowIteratorInterface;

}