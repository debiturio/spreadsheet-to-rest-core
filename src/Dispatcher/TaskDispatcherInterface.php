<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Dispatcher;

use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;

interface TaskDispatcherInterface
{
    public function dispatch(TaskEnvelope $taskEnvelope): void;
}