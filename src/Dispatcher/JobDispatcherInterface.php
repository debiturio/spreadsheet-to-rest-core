<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Dispatcher;


use Debiturio\SpreadsheetToRestCore\Model\JobInterface;

interface JobDispatcherInterface
{
    public function dispatch(JobInterface $job): void;
}