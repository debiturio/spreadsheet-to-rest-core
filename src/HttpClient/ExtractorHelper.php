<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\HttpClient;

use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentInterface;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;
use Laminas\Filter\FilterChain;

trait ExtractorHelper
{
    public static function getValuesFromRow(RowInterface $row, array $indexes): array
    {
        $result = [];

        foreach ($indexes as $index) {
            $value = $row->getCellByColumnIndex($index)->getValue();
            if ($value) $result[] = $value;
        }

        return $result;
    }

    public static function getConcatenatedValuesFromRow(RowInterface $row, AssignmentInterface $assignment): string|int|bool|float
    {
        return self::filterValue(
            implode(
                self::getSeparatorFromConcatType($assignment->getConcatType()),
                self::getValuesFromRow($row, $assignment->getColumnIndexes())
            ),
            $assignment
        );
    }

    public static function filterValue(mixed $value, AssignmentInterface $assignment): string|int|bool|float
    {
        $filterChain = new FilterChain();

        foreach ($assignment->getValueReplaceFilters() as $valueReplaceFilter) {
            $filterChain->attach($valueReplaceFilter);
        }

        $value = $filterChain->filter($value);

        return match ($assignment->getDataType()) {
            DataType::BOOLEAN => (bool) $value,
            DataType::INTEGER => (int) $value,
            DataType::NUMBER => (float) $value,
            default => (string) $value,
        };
    }

    public static function setArrayRecursive(array &$array, array $path, mixed $value): void
    {
        $key = str_replace(' ', '_', (string) array_shift($path));

        if (str_ends_with($key, ']') && str_contains($key, '[')) {
            $start = strpos($key, '[');
            $pathKey = substr($key, ($start + 1), -1);
            array_unshift($path, is_numeric($pathKey) ? (int)$pathKey : $pathKey);
            $key = substr($key, 0, $start);
        }

        if (empty($path)) {
            $array[$key] = $value;
        } else {
            if (!array_key_exists($key, $array) || !is_array($array[$key])) {
                $array[$key] = [];
            }
            self::setArrayRecursive($array[$key], $path, $value);
        }
    }

    /** deprecated */
    public static function nestedArrayToParams(array $input, array &$keys, array &$results): void
    {
        array_walk($input, function ($item, $itemKey) use (&$results, &$keys) {

            $keys[] = $itemKey;

            if (is_array($item)) {
                self::nestedArrayToParams($item, $keys, $results);
            } else {

                $wrappedKeys = array_map(
                    function ($value, $index) {
                        return $index > 0 ? sprintf('[%s]', $value) : $value;
                    },
                    $keys,
                    array_keys($keys)
                );

                $results[] = sprintf('%s=%s', implode('', $wrappedKeys), $item);
                $keys = [array_shift($keys)];
            }

        });
    }

    public static function getSeparatorFromConcatType(ConcatType $concatType): string
    {
        return match ($concatType) {
            ConcatType::SEMICOLON => ';',
            ConcatType::DOT => '.',
            ConcatType::WHITESPACE => ' ',
            default => ','
        };
    }
}