<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\HttpClient;

use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;

class PayloadExtractor
{
    use ExtractorHelper;

    public function extract(TaskEnvelope $taskEnvelope): array
    {
        $result = [];

        foreach ($taskEnvelope->getTask()->getJob()->getAssignments() as $assignment) {

            if ($assignment->getType() !== AssignmentType::PAYLOAD) continue;

            self::setArrayRecursive(
                $result,
                str_getcsv($assignment->getKey(), '.'),
                self::getConcatenatedValuesFromRow($taskEnvelope->getRow(), $assignment)
            );

        }

        return $result;
    }

}