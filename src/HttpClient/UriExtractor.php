<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\HttpClient;

use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;

class UriExtractor
{
    use ExtractorHelper;

    public function extract(TaskEnvelope $taskEnvelope): string
    {
        $uri = $taskEnvelope->getTask()->getJob()->getEndpoint()->getUri();

        $paramParts = [];

        foreach ($taskEnvelope->getTask()->getJob()->getAssignments() as $assignment) {

            if ($assignment->getType() === AssignmentType::PATH && str_contains($uri, '{')) {
                $value = self::getConcatenatedValuesFromRow($taskEnvelope->getRow(), $assignment);
                if(!empty($value)) $uri = str_replace(sprintf('{%s}', $assignment->getKey()), $value, $uri);
            }

            if ($assignment->getType() === AssignmentType::PARAM) {

                $value = self::getConcatenatedValuesFromRow($taskEnvelope->getRow(), $assignment);
                $keyPathParts = str_getcsv($assignment->getKey(), '.');

                $wrappedKeys = count($keyPathParts) > 0 ? implode('',
                    array_map(
                        function ($value, $index) {
                            return $index > 0 ? sprintf('[%s]', $value) : $value;
                            },
                        $keyPathParts,
                        array_keys($keyPathParts)
                    )
                ) : $keyPathParts;

                if (!empty($value)) $paramParts[] = sprintf('%s=%s', $wrappedKeys, $value);
            }

        }

        return $uri . (count($paramParts) > 0 ? '?' . implode('&', $paramParts) : '');
    }

}