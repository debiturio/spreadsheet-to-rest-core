<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\HttpClient;


use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class DefaultRequestFactory implements RequestFactoryInterface
{
    public function __construct(
        private UriExtractor $uriExtractor,
        private HeaderExtractor $headerExtractor,
        private PayloadExtractor $payloadExtractor)
    {
    }

    public function createRequest(TaskEnvelope $taskEnvelope): RequestInterface
    {
        $headers = array_replace_recursive(
            $taskEnvelope->getTask()->getJob()->getDefaultHeaders(),
            $this->headerExtractor->extract($taskEnvelope)
        );

        // TODO make alternative body content type possible
        if (!array_key_exists('Content-Type', $headers)) $headers['Content-Type'] = 'application/json';

        return new Request(
            $taskEnvelope->getTask()->getJob()->getEndpoint()->getMethod(),
            $this->uriExtractor->extract($taskEnvelope),
            $headers,
            json_encode(array_replace_recursive(
                $taskEnvelope->getTask()->getJob()->getDefaultPayload(),
                $this->payloadExtractor->extract($taskEnvelope)
            ))
        );
    }

}