<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\HttpClient;

use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Psr\Http\Message\RequestInterface;

interface RequestFactoryInterface
{
    public function createRequest(TaskEnvelope $taskEnvelope): RequestInterface;
}