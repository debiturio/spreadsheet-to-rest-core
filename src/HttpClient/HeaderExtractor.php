<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\HttpClient;

use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;

class HeaderExtractor
{
    use ExtractorHelper;

    public function extract(TaskEnvelope $taskEnvelope): array
    {
        $result = [];

        foreach ($taskEnvelope->getTask()->getJob()->getAssignments() as $assignment) {

            if ($assignment->getType() !== AssignmentType::HEADER) continue;

            $value = self::getConcatenatedValuesFromRow($taskEnvelope->getRow(), $assignment);

            if (!empty($value)) $result[$assignment->getKey()] = $value;

        }

        return $result;
    }

}