<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Handler;


use Debiturio\SpreadsheetToRestCore\Model\Factory\TaskRequestFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Factory\TaskResponseFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\TaskRequestRepositoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\TaskResponseRepositoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;

class TaskHandler
{

    public function __construct(
        private ClientInterface $client,
        private TaskRequestFactoryInterface $taskRequestFactory,
        private TaskRequestRepositoryInterface $taskRequestRepository,
        private TaskResponseFactoryInterface $taskResponseFactory,
        private TaskResponseRepositoryInterface $taskResponseRepository
    ) {
    }

    public function handle(TaskEnvelope $taskEnvelope): void
    {
        if ($taskEnvelope->getTask()->getJob()->isCanceled()) return;

        $taskRequest = $this->taskRequestFactory->create($taskEnvelope);
        $this->taskRequestRepository->save($taskRequest);

        try {
            $response = $this->client->sendRequest($taskRequest->getRequest());
        } catch (ClientExceptionInterface $e) {
            // TODO catch and add error to task
        }

        if (isset($response)) {
            $taskResponse = $this->taskResponseFactory->create($response, $taskEnvelope->getTask());
            $this->taskResponseRepository->save($taskResponse);
        }
    }

}