<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Handler;


use Debiturio\SpreadsheetToRestCore\Dispatcher\TaskDispatcherInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\HeadlineMapExtractor;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\JobRepositoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\Factory\TaskFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\TaskRepositoryInterface;

class JobHandler
{
    public function __construct(
        private TaskDispatcherInterface               $taskDispatcher,
        private TaskRepositoryInterface               $taskRepository,
        private TaskFactoryInterface                  $taskFactory,
        private SpreadsheetFileReaderFactoryInterface $fileReaderFactory,
        private JobRepositoryInterface                $jobRepository
    ) {
    }

    public function handle(JobInterface $job): void
    {
        $reader = $this->fileReaderFactory->getReader($job->getFile()->getPath());

        $numberOfReads = ceil($reader->getTotalRows() / $job->getRowsPerRead());

        if ($job->isCanceled()) return;

        for ($i = 1; $i <= $numberOfReads; $i++) {

            $latestJobState = $this->jobRepository->get($job->getId());

            if ($latestJobState->isCanceled()) return;

            foreach ($reader->getRows(($i - 1) *  $job->getRowsPerRead(), $job->getRowsPerRead()) as $row) {

                if ($row->getIndex() === 1 && $job->getFile()->isFirstRowHeader()) continue;

                $task = $this->taskFactory->create($latestJobState, $row->getIndex());
                $this->taskRepository->save($task);

                $this->taskDispatcher->dispatch(new TaskEnvelope($task, $row));
            }

        }

    }
}