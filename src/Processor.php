<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore;


use Debiturio\SpreadsheetToRestCore\Dispatcher\JobDispatcherInterface;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;

class Processor
{
    public function __construct(private JobDispatcherInterface $jobDispatcher)
    {
    }

    public function process(JobInterface $job)
    {
        $this->jobDispatcher->dispatch($job);
    }
}