<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;

use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;

interface AssignmentInterface
{
    /**
     * @return int[]
     */
    public function getColumnIndexes(): array;

    public function getKey(): string;

    public function getType(): AssignmentType;

    public function getConcatType(): ConcatType;

    public function getDataType(): DataType;

    public function get();

    public function getConcatOnlyUnique(): bool;

    public function getValueReplaceFilters(): array;
}