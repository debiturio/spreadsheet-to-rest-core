<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Factory;


use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;

interface TaskFactoryInterface
{
    public function create(JobInterface $job, int $rowIndex): TaskInterface;
}