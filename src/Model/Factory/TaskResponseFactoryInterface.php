<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Factory;

use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskResponseInterface;
use Psr\Http\Message\ResponseInterface;

interface TaskResponseFactoryInterface
{
    public function create(ResponseInterface $response, TaskInterface $task): TaskResponseInterface;
}