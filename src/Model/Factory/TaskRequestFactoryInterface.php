<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Factory;

use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskRequestInterface;

interface TaskRequestFactoryInterface
{
    public function create(TaskEnvelope $taskEnvelope): TaskRequestInterface;
}