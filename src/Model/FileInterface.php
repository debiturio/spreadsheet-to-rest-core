<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;


interface FileInterface
{
    public function getPath(): string;

    public function isFirstRowHeader(): bool;

    public function getNumberOfRows(): int;
}