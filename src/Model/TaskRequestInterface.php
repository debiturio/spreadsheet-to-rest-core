<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;

use Psr\Http\Message\RequestInterface;
use Ramsey\Uuid\UuidInterface;

interface TaskRequestInterface
{
    public function getId(): UuidInterface;

    public function getRequest(): RequestInterface;

    public function getCreatedAt(): \DateTime;
}