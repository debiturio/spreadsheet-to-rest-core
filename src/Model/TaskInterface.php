<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;

use Ramsey\Uuid\UuidInterface;

interface TaskInterface
{
    public function getJob(): JobInterface;

    public function getId(): UuidInterface;

    public function getRowIndex(): int;

    public function getTaskRequest(): ?TaskRequestInterface;

    public function setTaskRequest(TaskRequestInterface $taskRequest): void;

    public function getTaskResponse(): ?TaskResponseInterface;

    public function setTaskResponse(TaskResponseInterface $taskResponse): void;

}