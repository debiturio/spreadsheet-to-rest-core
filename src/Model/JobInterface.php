<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;


use Ramsey\Uuid\UuidInterface;

interface JobInterface
{
    public function getId(): UuidInterface;

    public function getFile(): FileInterface;

    public function getEndpoint(): EndpointInterface;

    public function getRowsPerRead(): int;

    public function getAssignments(): AssignmentIterator;

    public function isCanceled(): bool;

    public function getDefaultPayload(): array;

    public function getDefaultHeaders(): array;
}