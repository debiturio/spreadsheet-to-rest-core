<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Repository;

use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Ramsey\Uuid\UuidInterface;

interface JobRepositoryInterface
{
    public function save(JobInterface $job): void;

    public function get(UuidInterface $id): ?JobInterface;
}