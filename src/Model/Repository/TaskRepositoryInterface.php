<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Repository;


use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Ramsey\Uuid\UuidInterface;

interface TaskRepositoryInterface
{

    public function save(TaskInterface $task): void;

    public function get(UuidInterface $id): ?TaskInterface;
}