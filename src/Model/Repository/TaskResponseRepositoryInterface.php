<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Repository;

use Debiturio\SpreadsheetToRestCore\Model\TaskResponseInterface;

interface TaskResponseRepositoryInterface
{
    public function save(TaskResponseInterface $response): void;
}