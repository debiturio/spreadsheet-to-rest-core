<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Repository;

use Debiturio\SpreadsheetToRestCore\Model\TaskRequestInterface;

interface TaskRequestRepositoryInterface
{
    public function save(TaskRequestInterface $taskRequest): void;
}