<?php
declare(strict_types=1);


namespace Debiturio\SpreadsheetToRestCore\Model;


interface EndpointInterface
{
    public function getMethod(): string;

    public function getUri(): string;
}