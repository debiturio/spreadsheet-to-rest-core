<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;

use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\UuidInterface;

interface TaskResponseInterface
{
    public function getId(): UuidInterface;

    public function getResponse(): ResponseInterface;

    public function getStatusCode(): int;

    public function getCreatedAt(): \DateTime;
}