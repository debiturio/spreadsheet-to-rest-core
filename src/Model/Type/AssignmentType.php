<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Type;
// TODO replace MapType by different Fieldnterfaces -> MapTargetInterfaces
enum AssignmentType: string
{
    case PATH = 'path';
    case PARAM = 'param';
    case HEADER = 'header';
    case PAYLOAD = 'payload';
}