<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Type;

enum ConcatType: string
{
    case COMMA = 'comma';
    case SEMICOLON = 'semicolon';
    case DOT = 'dot';
    case WHITESPACE = 'whitespace';
}