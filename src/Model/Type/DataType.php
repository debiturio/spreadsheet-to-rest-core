<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model\Type;

enum DataType: string
{
    case STRING = 'string';
    case NUMBER = 'number';
    case INTEGER = 'integer';
    case BOOLEAN = 'boolean';
}