<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;

class AssignmentIterator implements \Iterator
{
    /** @var string[] */
    private array $uniqueHashValues = [];

    /** @var AssignmentInterface[] */
    private array $assignments = [];

    /** @var int */
    private int $key = 0;

    public function __construct(array $assignments)
    {
        /** @var AssignmentInterface $assignment */
        foreach ($assignments as $assignment) {

            $hash = md5($assignment->getKey() . $assignment->getType()->value);

            if (in_array($hash, $this->uniqueHashValues)) continue;

            $this->uniqueHashValues[] = $hash;
            $this->assignments[] = $assignment;

        }
    }

    public function current(): AssignmentInterface
    {
        return $this->assignments[$this->key];
    }

    public function toArray(): array
    {
        return $this->assignments;
    }

    public function next(): void
    {
        $this->key++;
    }

    public function key(): int
    {
        return $this->key();
    }

    public function valid(): bool
    {
        return array_key_exists($this->key, $this->assignments);
    }

    public function rewind(): void
    {
        $this->key = 0;
    }
}