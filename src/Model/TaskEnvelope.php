<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Model;

use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;

class TaskEnvelope
{
    public function __construct(private TaskInterface $task, private RowInterface $row)
    {
    }

    /**
     * @return TaskInterface
     */
    public function getTask(): TaskInterface
    {
        return $this->task;
    }

    /**
     * @return RowInterface
     */
    public function getRow(): RowInterface
    {
        return $this->row;
    }
}