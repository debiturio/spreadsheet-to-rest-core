<?php
declare(strict_types=1);

namespace Debiturio\SpreadsheetToRestCore\Filter;

use Laminas\Filter\FilterInterface;

class ValueReplaceFilter implements FilterInterface
{
    public function __construct(private string $search, private string $replace)
    {
    }

    public function filter($value): mixed
    {
        $stringValue = (string) $value;

        if ($stringValue === $this->search) return $this->replace;

        return $value;
    }
}