<?php

namespace Debiturio\SpreadsheetToRestCoreTest\Model;

use Debiturio\SpreadsheetToRestCore\Model\AssignmentInterface;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentIterator;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;
use PHPUnit\Framework\TestCase;

class AssignmentIteratorTest extends TestCase
{

    public function test__construct()
    {

        $assignmentOne = $this->createMock(AssignmentInterface::class);
        $assignmentOne->method('getType')->willReturn(AssignmentType::PAYLOAD);
        $assignmentOne->method('getDataType')->willReturn(DataType::STRING);

        $assignmentTwo = $this->createMock(AssignmentInterface::class);
        $assignmentTwo->method('getType')->willReturn(AssignmentType::HEADER);
        $assignmentTwo->method('getDataType')->willReturn(DataType::STRING);

        $iterator = new AssignmentIterator($assignments = [$assignmentOne, $assignmentTwo]);

        $result = [];

        foreach ($iterator as $item) $result[] = $item;

        $this->assertEquals($assignments, $result);

        $this->assertEquals([$assignmentOne, $assignmentTwo], $iterator->toArray());

    }
}
