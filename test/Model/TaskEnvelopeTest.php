<?php

namespace Debiturio\SpreadsheetToRestCoreTest\Model;

use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use PHPUnit\Framework\TestCase;

class TaskEnvelopeTest extends TestCase
{

    public function test__construct()
    {
        $row = $this->createMock(RowInterface::class);
        $task = $this->createMock(TaskInterface::class);

        $env = new TaskEnvelope($task, $row);

        $this->assertEquals($row, $env->getRow());
        $this->assertEquals($task, $env->getTask());
    }
}
