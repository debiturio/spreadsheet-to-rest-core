<?php

namespace Debiturio\SpreadsheetToRestCoreTest\HttpClient;

use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\HttpClient\UriExtractor;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentInterface;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentIterator;
use Debiturio\SpreadsheetToRestCore\Model\EndpointInterface;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;
use PHPUnit\Framework\TestCase;

class UriExtractorTest extends TestCase
{
    public function testExtract()
    {
        $assignmentOne = $this->createMock(AssignmentInterface::class);
        $assignmentOne->method('getKey')->willReturn('id');
        $assignmentOne->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentOne->method('getColumnIndexes')->willReturn([1]);
        $assignmentOne->method('getType')->willReturn(AssignmentType::PATH);
        $assignmentOne->method('getDataType')->willReturn(DataType::STRING);

        $assignmentTwo = $this->createMock(AssignmentInterface::class);
        $assignmentTwo->method('getKey')->willReturn('foo');
        $assignmentTwo->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentTwo->method('getColumnIndexes')->willReturn([2]);
        $assignmentTwo->method('getType')->willReturn(AssignmentType::PARAM);
        $assignmentTwo->method('getDataType')->willReturn(DataType::STRING);

        $assignmentThree = $this->createMock(AssignmentInterface::class);
        $assignmentThree->method('getKey')->willReturn('bar.bar.x');
        $assignmentThree->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentThree->method('getColumnIndexes')->willReturn([3,4]);
        $assignmentThree->method('getType')->willReturn(AssignmentType::PARAM);
        $assignmentThree->method('getDataType')->willReturn(DataType::STRING);

        $assignmentIterator = new AssignmentIterator([$assignmentOne, $assignmentTwo, $assignmentThree]);

        $cellOne = $this->createMock(CellInterface::class);
        $cellOne->method('getColumnIndex')->willReturn(1);
        $cellOne->method('getValue')->willReturn('123');
        $cellTwo = $this->createMock(CellInterface::class);
        $cellTwo->method('getColumnIndex')->willReturn(2);
        $cellTwo->method('getValue')->willReturn('bar');
        $cellThree = $this->createMock(CellInterface::class);
        $cellThree->method('getColumnIndex')->willReturn(3);
        $cellThree->method('getValue')->willReturn('foo');
        $cellFour = $this->createMock(CellInterface::class);
        $cellFour->method('getColumnIndex')->willReturn(4);
        $cellFour->method('getValue')->willReturn('baz');

        $row = $this->createMock(RowInterface::class);
        $row->method('getCellByColumnIndex')
            ->withConsecutive([1], [2], [3], [4])
            ->willReturnOnConsecutiveCalls($cellOne, $cellTwo, $cellThree, $cellFour);


        $endpoint = $this->createMock(EndpointInterface::class);
        $endpoint->method('getUri')->willReturn('https://test.debiturio.com/path/{id}');

        $job = $this->createMock(JobInterface::class);
        $job->method('getAssignments')->willReturn($assignmentIterator);
        $job->method('getEndpoint')->willReturn($endpoint);

        $task = $this->createMock(TaskInterface::class);
        $task->method('getJob')->willReturn($job);

        $taskEnvelope = $this->createMock(TaskEnvelope::class);
        $taskEnvelope->method('getTask')->willReturn($task);
        $taskEnvelope->method('getRow')->willReturn($row);

        $extractor = new UriExtractor();

        $this->assertEquals(
            'https://test.debiturio.com/path/123?foo=bar&bar[bar][x]=foo,baz',
            $extractor->extract($taskEnvelope)
        );

    }
}
