<?php

namespace Debiturio\SpreadsheetToRestCoreTest\HttpClient;

use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\HttpClient\HeaderExtractor;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentInterface;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentIterator;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;
use PHPUnit\Framework\TestCase;

class HeaderExtractorTest extends TestCase
{
    public function testExtract()
    {
        $assignmentOne = $this->createMock(AssignmentInterface::class);
        $assignmentOne->method('getKey')->willReturn('auth');
        $assignmentOne->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentOne->method('getColumnIndexes')->willReturn([1,2]);
        $assignmentOne->method('getType')->willReturn(AssignmentType::HEADER);
        $assignmentOne->method('getDataType')->willReturn(DataType::STRING);

        $assignmentTwo = $this->createMock(AssignmentInterface::class);
        $assignmentTwo->method('getKey')->willReturn('test');
        $assignmentTwo->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentTwo->method('getColumnIndexes')->willReturn([2]);
        $assignmentTwo->method('getType')->willReturn(AssignmentType::HEADER);
        $assignmentTwo->method('getDataType')->willReturn(DataType::BOOLEAN);

        $assignmentIterator = new AssignmentIterator([$assignmentOne, $assignmentTwo]);

        $cellOne = $this->createMock(CellInterface::class);
        $cellOne->method('getColumnIndex')->willReturn(1);
        $cellOne->method('getValue')->willReturn('123456789');
        $cellTwo = $this->createMock(CellInterface::class);
        $cellTwo->method('getColumnIndex')->willReturn(2);
        $cellTwo->method('getValue')->willReturn('true');

        $row = $this->createMock(RowInterface::class);
        $row->method('getCellByColumnIndex')
            ->withConsecutive([1], [2], [2])
            ->willReturnOnConsecutiveCalls($cellOne, $cellTwo, $cellTwo);

        $job = $this->createMock(JobInterface::class);
        $job->method('getAssignments')->willReturn($assignmentIterator);

        $task = $this->createMock(TaskInterface::class);
        $task->method('getJob')->willReturn($job);

        $taskEnvelope = $this->createMock(TaskEnvelope::class);
        $taskEnvelope->method('getTask')->willReturn($task);
        $taskEnvelope->method('getRow')->willReturn($row);

        $extractor = new HeaderExtractor();
        $headers = $extractor->extract($taskEnvelope);

        $this->assertEquals(
            [
                'auth' => '123456789,true',
                'test' => true
            ],
            $headers
        );

    }
}
