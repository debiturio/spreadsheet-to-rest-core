<?php

namespace Debiturio\SpreadsheetToRestCoreTest\HttpClient;

use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\Filter\ValueReplaceFilter;
use Debiturio\SpreadsheetToRestCore\HttpClient\ExtractorHelper;
use Debiturio\SpreadsheetToRestCore\HttpClient\PayloadExtractor;
use Debiturio\SpreadsheetToRestCore\HttpClient\UriExtractor;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentInterface;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;
use PHPUnit\Framework\TestCase;

class ExtractorHelperTest extends TestCase
{
    use ExtractorHelper;

    /**
     * @dataProvider dataProviderTestFilterValue
     * @param mixed $value
     * @param DataType $dataType
     * @param mixed $expected
     */
    public function testFilterValue(mixed $value, DataType $dataType, mixed $expected)
    {
        $assignment = $this->createMock(AssignmentInterface::class);
        $assignment->method('getDataType')->willReturn($dataType);

        $filter = $this->createMock(ValueReplaceFilter::class);
        $filter->expects($this->once())->method('filter')->with($value)->willReturn($value);

        $assignment->method('getValueReplaceFilters')->willReturn([$filter]);

        $this->assertEquals(
            $expected,
            ExtractorHelper::filterValue($value, $assignment)
        );
    }

    public function dataProviderTestFilterValue()
    {
        return [
            [
                1,
                DataType::INTEGER,
                1
            ],
            [
                200,
                DataType::STRING,
                '200'
            ],
            [
                1.845,
                DataType::NUMBER,
                '1.845'
            ],
            [
                1,
                DataType::BOOLEAN,
                true
            ],
            [
                0,
                DataType::BOOLEAN,
                false
            ],
            [
                'true',
                DataType::BOOLEAN,
                true
            ]
        ];
    }


    public function testGetValuesFromRow()
    {
        $indexes = [1,4];
        $row = $this->createMock(RowInterface::class);

        $cellOne = $this->createMock(CellInterface::class);
        $cellOne->method('getValue')->willReturn('foo');

        $cellTwo = $this->createMock(CellInterface::class);
        $cellTwo->method('getValue')->willReturn('bar');

        $row->method('getCellByColumnIndex')
            ->withConsecutive([1], [4])
            ->willReturnOnConsecutiveCalls($cellOne, $cellTwo);

        $this->assertEquals(
            [
                'foo',
                'bar'
            ],
            self::getValuesFromRow($row, $indexes)
        );
    }

    public function testGetSeparatorFromConcatType()
    {
        $this->assertEquals('.', self::getSeparatorFromConcatType(ConcatType::DOT));
    }

    public function testGetConcatenatedValuesFromRow()
    {
        $assignment = $this->createMock(AssignmentInterface::class);
        $assignment->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignment->method('getColumnIndexes')->willReturn([1,2]);
        $assignment->method('getDataType')->willReturn(DataType::STRING);

        $cellOne = $this->createMock(CellInterface::class);
        $cellOne->method('getValue')->willReturn('foo');

        $cellTwo = $this->createMock(CellInterface::class);
        $cellTwo->method('getValue')->willReturn('bar');

        $row = $this->createMock(RowInterface::class);
        $row->method('getCellByColumnIndex')
            ->withConsecutive([1], [2])
            ->willReturnOnConsecutiveCalls($cellOne, $cellTwo);

        $this->assertEquals('foo,bar', self::getConcatenatedValuesFromRow($row, $assignment));

    }

    public function testNestedArrayToParams()
    {

        $input = [
            'one' => [
                'two' => [
                    'three' => 'foo'
                ]
            ],
            'a' => 'bar'
        ];

        $keys = ['foo'];
        $results = [];

        UriExtractor::nestedArrayToParams($input, $keys, $results);

        $this->assertEquals(
            [
                'foo[one][two][three]=foo',
                'foo[a]=bar'
            ],
            $results
        );

    }

    /**
     * @dataProvider setArrayRecursiveProvider
     * @param array $input
     * @param array $path
     * @param mixed $value
     * @param array $expected
     */
    public function testSetArrayRecursive(array $input, array $path, mixed $value, array $expected)
    {
        PayloadExtractor::setArrayRecursive($input, $path, $value);
        $this->assertEquals($expected, $input);
    }

    public function setArrayRecursiveProvider(): array
    {
        return [
            [
                [
                    'user' => [
                        'address' => [
                            'street' => 'Street 1'
                        ]
                    ]
                ],
                ['user', 'address', 'postcode'],
                20000,
                [
                    'user' => [
                        'address' => [
                            'street' => 'Street 1',
                            'postcode' => 20000
                        ]
                    ]
                ]
            ],
            [
                [
                    'users' => [
                        [
                            'address' => [
                                'street' => 'Street 1'
                            ]
                        ]
                    ]
                ],
                ['users', '0', 'address', 'postcode'],
                20000,
                [
                    'users' => [
                        [
                            'address' => [
                                'street' => 'Street 1',
                                'postcode' => 20000
                            ]
                        ]
                    ]
                ]
            ],
            [
                [
                    'users' => [
                        [
                            'addresses' => [
                                [
                                    'street' => 'Street 2',
                                    'postcode' => 30000
                                ],
                                [
                                    'street' => 'Street 1',
                                ]
                            ]
                        ]
                    ]
                ],
                ['users', '0', 'addresses', '1', 'postcode'],
                20000,
                [
                    'users' => [
                        [
                            'addresses' => [
                                [
                                    'street' => 'Street 2',
                                    'postcode' => 30000
                                ],
                                [
                                    'street' => 'Street 1',
                                    'postcode' => 20000
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
