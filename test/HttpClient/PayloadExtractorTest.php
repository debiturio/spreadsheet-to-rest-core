<?php

namespace Debiturio\SpreadsheetToRestCoreTest\HttpClient;

use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\HttpClient\PayloadExtractor;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentInterface;
use Debiturio\SpreadsheetToRestCore\Model\AssignmentIterator;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Debiturio\SpreadsheetToRestCore\Model\Type\ConcatType;
use Debiturio\SpreadsheetToRestCore\Model\Type\AssignmentType;
use Debiturio\SpreadsheetToRestCore\Model\Type\DataType;
use PHPUnit\Framework\TestCase;

class PayloadExtractorTest extends TestCase
{
    public function testExtract()
    {
        $assignmentOne = $this->createMock(AssignmentInterface::class);
        $assignmentOne->method('getKey')->willReturn('foo.bar');
        $assignmentOne->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentOne->method('getColumnIndexes')->willReturn([1,2]);
        $assignmentOne->method('getType')->willReturn(AssignmentType::PAYLOAD);
        $assignmentOne->method('getDataType')->willReturn(DataType::STRING);

        $assignmentTwo = $this->createMock(AssignmentInterface::class);
        $assignmentTwo->method('getKey')->willReturn('foo."bar.x"');
        $assignmentTwo->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentTwo->method('getColumnIndexes')->willReturn([2]);
        $assignmentTwo->method('getType')->willReturn(AssignmentType::PAYLOAD);
        $assignmentTwo->method('getDataType')->willReturn(DataType::STRING);

        $assignmentThree = $this->createMock(AssignmentInterface::class);
        $assignmentThree->method('getKey')->willReturn('list[0].color');
        $assignmentThree->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentThree->method('getColumnIndexes')->willReturn([2]);
        $assignmentThree->method('getType')->willReturn(AssignmentType::PAYLOAD);
        $assignmentThree->method('getDataType')->willReturn(DataType::STRING);

        $assignmentFour = $this->createMock(AssignmentInterface::class);
        $assignmentFour->method('getKey')->willReturn('list[1].color');
        $assignmentFour->method('getConcatType')->willReturn(ConcatType::COMMA);
        $assignmentFour->method('getColumnIndexes')->willReturn([1]);
        $assignmentFour->method('getType')->willReturn(AssignmentType::PAYLOAD);
        $assignmentFour->method('getDataType')->willReturn(DataType::STRING);

        $assignmentIterator = new AssignmentIterator([$assignmentOne, $assignmentTwo, $assignmentThree, $assignmentFour]);

        $cellOne = $this->createMock(CellInterface::class);
        $cellOne->method('getValue')->willReturn('foo');
        $cellOne->method('getColumnIndex')->willReturn(1);
        $cellTwo = $this->createMock(CellInterface::class);
        $cellTwo->method('getColumnIndex')->willReturn(2);
        $cellTwo->method('getValue')->willReturn('bar');

        $row = $this->createMock(RowInterface::class);
        $row->method('getCellByColumnIndex')
            ->withConsecutive([1], [2], [2], [2], [1])
            ->willReturnOnConsecutiveCalls($cellOne, $cellTwo, $cellTwo, $cellTwo, $cellOne);

        $job = $this->createMock(JobInterface::class);
        $job->method('getAssignments')->willReturn($assignmentIterator);

        $task = $this->createMock(TaskInterface::class);
        $task->method('getJob')->willReturn($job);

        $taskEnvelope = $this->createMock(TaskEnvelope::class);
        $taskEnvelope->method('getTask')->willReturn($task);
        $taskEnvelope->method('getRow')->willReturn($row);

        $extractor = new PayloadExtractor();

        $this->assertEquals(
            [
                'foo' => [
                    'bar' => 'foo,bar',
                    'bar.x' => 'bar'
                ],
                'list' => [
                    ['color' => 'bar'],
                    ['color' => 'foo'],
                ]
            ],
            $extractor->extract($taskEnvelope)
        );

    }
}
