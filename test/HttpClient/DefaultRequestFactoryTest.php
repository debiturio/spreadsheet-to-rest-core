<?php

namespace Debiturio\SpreadsheetToRestCoreTest\HttpClient;

use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\CellIteratorInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\HttpClient\DefaultRequestFactory;
use Debiturio\SpreadsheetToRestCore\HttpClient\HeaderExtractor;
use Debiturio\SpreadsheetToRestCore\HttpClient\PayloadExtractor;
use Debiturio\SpreadsheetToRestCore\HttpClient\UriExtractor;
use Debiturio\SpreadsheetToRestCore\Model\EndpointInterface;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use GuzzleHttp\Psr7\Utils;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DefaultRequestFactoryTest extends TestCase
{

    public function testCreateRequest()
    {
        $uriExtractor = $this->createMock(UriExtractor::class);
        $headerExtractor = $this->createMock(HeaderExtractor::class);
        $payloadExtractor = $this->createMock(PayloadExtractor::class);

        $endpoint = $this->createMock(EndpointInterface::class);
        $endpoint->method('getMethod')->willReturn('POST');

        $job = $this->createMock(JobInterface::class);
        $job->method('getEndpoint')->willReturn($endpoint);
        $job->method('getDefaultHeaders')->willReturn(['Content-Type' => 'application/json']);
        $job->method('getDefaultPayload')->willReturn(['b' => '00', 'c' => [ 'one' => 'foo', 'two' => 'bar']]);

        $task = $this->createMock(TaskInterface::class);
        $task->method('getJob')->willReturn($job);

        $taskEnvelope = new TaskEnvelope($task, $this->createMock(RowInterface::class));

        $uriExtractor->expects($this->once())->method('extract')->with($taskEnvelope)->willReturn('uri');
        $headerExtractor->expects($this->once())->method('extract')
            ->with($taskEnvelope)->willReturn(['Test' => 'true']);
        $payloadExtractor->expects($this->once())->method('extract')
            ->with($taskEnvelope)->willReturn(['a' => 'foo', 'b' => 'bar', 'c' => [ 'one' => 'x']]);

        $factory = new DefaultRequestFactory(
            $uriExtractor,
            $headerExtractor,
            $payloadExtractor
        );

        $request = $factory->createRequest($taskEnvelope);

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('uri', $request->getUri());
        $this->assertEquals(['application/json'], $request->getHeader('Content-Type'));
        $this->assertEquals(['true'], $request->getHeader('Test'));
        $this->assertEquals(json_encode(['b' => 'bar', 'c' => [ 'one' => 'x', 'two' => 'bar'], 'a' => 'foo']), Utils::copyToString($request->getBody()));
    }

}
