<?php

namespace Debiturio\SpreadsheetToRestCoreTest;

use Debiturio\SpreadsheetToRestCore\Dispatcher\JobDispatcherInterface;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Processor;
use PHPUnit\Framework\TestCase;

class ProcessorTest extends TestCase
{

    public function testProcess()
    {
        $job = $this->createMock(JobInterface::class);

        $dispatcher = $this->createMock(JobDispatcherInterface::class);
        $dispatcher->expects($this->once())->method('dispatch')->with($job);

        $processor = new Processor($dispatcher);
        $processor->process($job);
    }
}
