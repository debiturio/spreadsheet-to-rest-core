<?php

namespace Debiturio\SpreadsheetToRestCoreTest\Handler;

use Debiturio\SpreadsheetToRestCore\Dispatcher\TaskDispatcherInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowIteratorInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderFactoryInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderInterface;
use Debiturio\SpreadsheetToRestCore\Handler\JobHandler;
use Debiturio\SpreadsheetToRestCore\Handler\TaskHandler;
use Debiturio\SpreadsheetToRestCore\Model\FileInterface;
use Debiturio\SpreadsheetToRestCore\Model\JobInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\JobRepositoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Debiturio\SpreadsheetToRestCore\Model\Factory\TaskFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\TaskRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

class JobHandlerTest extends TestCase
{

    public function testHandle()
    {
        $file = $this->createMock(FileInterface::class);
        $file->expects($this->once())->method('getPath')->willReturn($path = 'path/to/file');
        $file->expects($this->exactly(1))->method('isFirstRowHeader')->willReturn(true);

        $job = $this->createMock(JobInterface::class);
        $job->expects($this->exactly(2))->method('getId')->willReturn($jobId = $this->createMock(UuidInterface::class));
        $job->expects($this->exactly(1))->method('isCanceled')->willReturn(false);
        $job->expects($this->exactly(2))->method('getFile')->willReturn($file);

        $job->expects($this->atLeast(3))->method('getRowsPerRead')->willReturn(1);

        $rowOne = $this->createMock(RowInterface::class);
        $rowOne->expects($this->once())->method('getIndex')->willReturn(1);

        $rowTwo = $this->createMock(RowInterface::class);
        $rowTwo->expects($this->exactly(2))->method('getIndex')->willReturn(2);

        $rowIteratorOne = $this->createMock(RowIteratorInterface::class);
        $this->addItemsToIteratorMock($rowIteratorOne, [$rowOne]);
        $rowIteratorTwo = $this->createMock(RowIteratorInterface::class);
        $this->addItemsToIteratorMock($rowIteratorTwo, [$rowTwo]);

        $fileReader = $this->createMock(SpreadsheetFileReaderInterface::class);
        $fileReader->expects($this->once())->method('getTotalRows')->willReturn(2);
        $fileReader->expects($this->exactly(2))->method('getRows')
            ->willReturnOnConsecutiveCalls($rowIteratorOne, $rowIteratorTwo);

        $fileReaderFactory = $this->createMock(SpreadsheetFileReaderFactoryInterface::class);
        $fileReaderFactory->expects($this->once())->method('getReader')->with($path)->willReturn($fileReader);

        $task = $this->createMock(TaskInterface::class);

        $taskFactory = $this->createMock(TaskFactoryInterface::class);
        $taskFactory->method('create')->willReturn($task);


        $taskRepository = $this->createMock(TaskRepositoryInterface::class);
        $taskRepository->expects($this->once())->method('save')->with($task);

        $taskDispatcher = $this->createMock(TaskDispatcherInterface::class);
        $taskDispatcher->expects($this->once())->method('dispatch')->with(
            $this->callback(function (TaskEnvelope $taskEnvelope) use ($rowTwo, $task) {
                $this->assertEquals($rowTwo, $taskEnvelope->getRow());
                $this->assertEquals($task, $taskEnvelope->getTask());
                return true;
            })
        );

        $latestJob = $this->createMock(JobInterface::class);
        $latestJob->expects($this->exactly(2))->method('isCanceled')->willReturn(false);

        $jobRepository = $this->createMock(JobRepositoryInterface::class);
        $jobRepository->method('get')->with($jobId)->willReturn($latestJob);

        $handler = new JobHandler(
            $taskDispatcher,
            $taskRepository,
            $taskFactory,
            $fileReaderFactory,
            $jobRepository
        );
        $handler->handle($job);
    }

    function addItemsToIteratorMock(MockObject $iterator, array $items)
    {
        $iteratorData = new \stdClass();
        $iteratorData->items = $items;
        $iteratorData->position = 0;

        $iterator->expects($this->any())
            ->method('rewind')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        $iteratorData->position = 0;
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('current')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        return $iteratorData->items[$iteratorData->position];
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('key')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        return $iteratorData->position;
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('next')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        $iteratorData->position++;
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('valid')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        return isset($iteratorData->items[$iteratorData->position]);
                    }
                )
            );

        return $iterator;
    }
}
