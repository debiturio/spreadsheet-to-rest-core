<?php

namespace Debiturio\SpreadsheetToRestCoreTest\Handler;

use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\Handler\TaskHandler;
use Debiturio\SpreadsheetToRestCore\Model\Factory\TaskRequestFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Factory\TaskResponseFactoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\TaskRequestRepositoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\Repository\TaskResponseRepositoryInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskEnvelope;
use Debiturio\SpreadsheetToRestCore\Model\TaskInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskRequestInterface;
use Debiturio\SpreadsheetToRestCore\Model\TaskResponseInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class TaskHandlerTest extends TestCase
{

    public function testHandle()
    {
        $task = $this->createMock(TaskInterface::class);
        $taskEnvelope = new TaskEnvelope($task, $this->createMock(RowInterface::class));

        $request = $this->createMock(RequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);

        $taskRequest = $this->createMock(TaskRequestInterface::class);
        $taskRequest->method('getRequest')->willReturn($request);
        $taskResponse = $this->createMock(TaskResponseInterface::class);

        $taskRequestFactory = $this->createMock(TaskRequestFactoryInterface::class);
        $taskRequestFactory->method('create')->with($taskEnvelope)->willReturn($taskRequest);

        $taskResponseFactory = $this->createMock(TaskResponseFactoryInterface::class);
        $taskResponseFactory->method('create')->with($response)->willReturn($taskResponse);

        $taskRequestRepository = $this->createMock(TaskRequestRepositoryInterface::class);
        $taskRequestRepository->method('save')->with($taskRequest);

        $taskResponseRepository = $this->createMock(TaskResponseRepositoryInterface::class);
        $taskResponseRepository->method('save')->with($taskResponse);

        $client = $this->createMock(ClientInterface::class);
        $client->expects($this->once())->method('sendRequest')->with($request)->willReturn($response);

        $handler = new TaskHandler(
            $client,
            $taskRequestFactory,
            $taskRequestRepository,
            $taskResponseFactory,
            $taskResponseRepository
        );

        $handler->handle($taskEnvelope);
    }
}
