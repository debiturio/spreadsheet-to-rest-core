<?php

namespace Debiturio\SpreadsheetToRestCoreTest\FileReader;

use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\CellIteratorInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\HeadlineMapExtractor;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowIteratorInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class HeadlineMapExtractorTest extends TestCase
{

    public function testExtract()
    {
        $cellOne = $this->createMock(CellInterface::class);
        $cellOne->expects($this->once())->method('getColumnIndex')->willReturn(1);
        $cellOne->expects($this->once())->method('getValue')->willReturn($valueOne = 'foo');
        $cellTwo = $this->createMock(CellInterface::class);
        $cellTwo->expects($this->once())->method('getColumnIndex')->willReturn(2);
        $cellTwo->expects($this->once())->method('getValue')->willReturn($valueTwo = 'bar');

        $cells = $this->createMock(CellIteratorInterface::class);
        $this->addItemsToIteratorMock($cells, [$cellOne, $cellTwo]);

        $row = $this->createMock(RowInterface::class);
        $row->expects($this->once())->method('getCells')->willReturn($cells);

        $rows = $this->createMock(RowIteratorInterface::class);
        $this->addItemsToIteratorMock($rows, [$row]);

        $reader = $this->createMock(SpreadsheetFileReaderInterface::class);
        $reader->expects($this->once())->method('getRows')->with(2, 1)->willReturn($rows);

        $result = HeadlineMapExtractor::extract($reader, 2);

        $this->assertEquals([1 => 'foo', 2 => 'bar'], $result);

    }

    function addItemsToIteratorMock(MockObject $iterator, array $items)
    {
        $iteratorData = new \stdClass();
        $iteratorData->items = $items;
        $iteratorData->position = 0;

        $iterator->expects($this->any())
            ->method('rewind')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        $iteratorData->position = 0;
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('current')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        return $iteratorData->items[$iteratorData->position];
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('key')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        return $iteratorData->position;
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('next')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        $iteratorData->position++;
                    }
                )
            );

        $iterator->expects($this->any())
            ->method('valid')
            ->will(
                $this->returnCallback(
                    function() use ($iteratorData) {
                        return isset($iteratorData->items[$iteratorData->position]);
                    }
                )
            );

        return $iterator;
    }
}
